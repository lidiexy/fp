<?php
/**
 * Created by PhpStorm.
 * User: Lidiexy
 * Date: 8/12/2015
 * Time: 12:47 PM
 */
header("Access-Control-Allow-Origin: *");
//Send the email to FuzeNet Marketing Team

$errors = array();      // array to hold validation errors
$data = array();      // array to pass back data

date_default_timezone_set('America/Chicago');

// validate the variables ======================================================
if (empty($_POST['id']))
    $errors['id'] = 'ID is required.';
if (empty($_POST['firstname']))
    $errors['firstname'] = 'Fisrtname is required.';
if (empty($_POST['lastname']))
    $errors['lastname'] = 'Lastname is required.';
if (empty($_POST['organization']))
    $errors['organization'] = 'Organization is required.';
if (empty($_POST['city']))
    $errors['city'] = 'City is required.';
// return a response ===========================================================

// response if there are errors
if (!empty($errors)) {
    // if there are items in our errors array, return those errors
    $data['success'] = false;
    $data['errors']  = $errors;
    $data['message'] = 'Sorry, some issue occur in the process, try later.';
} else {
    $id = stripcslashes($_POST['id']);
    $firstname = stripcslashes($_POST['firstname']);
    $lastname = stripcslashes($_POST['lastname']);
    $organization = stripcslashes($_POST['organization']);
    $city = stripcslashes($_POST['city']);
    $email = stripcslashes($_POST['email']);
    $phone = stripcslashes($_POST['phone']);
    $present = false;
    $stamp = '';

    //Load the json and Write in it
    if($file_handler = file_get_contents('./data/vip.json')) {
        $vip_handler = json_decode($file_handler);
        $newPerson = array(
            "id"=> $id,
            "firstname"=> $firstname,
            "lastname"=> $lastname,
            "organization"=> $organization,
            "city"=> $city,
            "email"=> $email,
            "phone" => $phone,
            "present" => $present,
            "stamp" => $stamp
        );
        array_push($vip_handler, $newPerson);
        $jsonSaveData = json_encode($vip_handler, JSON_UNESCAPED_UNICODE);
        if(file_put_contents('./data/vip.json', $jsonSaveData)) {
            $data['success'] = true;
            $data['person'] = $newPerson;
            $data['message'] = 'Great!!, New VIP was added.';
        } else {
            $data['success'] = false;
            $data['message'] = 'Error writing the information to the user list.';
        }
    } else {
        $data['success'] = false;
        $data['message'] = 'Error reading the VIP list.';
    }
}

// return all our data to an AJAX call
echo json_encode($data);