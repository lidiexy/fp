<?php
/**
 * Created by PhpStorm.
 * User: Lidiexy
 * Date: 8/12/2015
 * Time: 12:47 PM
 */
header("Access-Control-Allow-Origin: *");
//Send the email to FuzeNet Marketing Team

$errors = array();      // array to hold validation errors
$data = array();      // array to pass back

date_default_timezone_set('America/Chicago');

// validate the variables ======================================================
if (empty($_POST['id']) || !is_numeric($_POST['id']))
    $errors['id'] = 'Bad Object.';
// return a response ===========================================================

// response if there are errors
if (!empty($errors)) {
    // if there are items in our errors array, return those errors
    $data['success'] = false;
    $data['errors']  = $errors;
    $data['message'] = 'Sorry, some issue occur in the process, try later.';
} else {
    //Load the json and Write in it
    if($file_handler = file_get_contents('./data/vip.json')) {
        $vip_handler = json_decode($file_handler);
        $theuser = array_filter($vip_handler, function($post) {
            return $post->id == $_POST['id'];
        });
        $key = key($theuser);
        //update the present field and the time of checking
        $vip_handler[$key]->present = true;
        $vip_handler[$key]->stamp = date('m-d-Y H:i:s');
        $jsonSaveData = json_encode($vip_handler, JSON_UNESCAPED_UNICODE);
        if(file_put_contents('./data/vip.json', stripcslashes($jsonSaveData))) {
            $data['success'] = true;
            $data['key'] = $key;
            $data['person'] = $vip_handler[$key];
            $data['message'] = 'Great!!, Checked and updated.';
        } else {
            $data['success'] = false;
            $data['message'] = 'Error writing the updated values to the user data.';
        }
    } else {
        $data['success'] = false;
        $data['message'] = 'Error reading the VIP list.';
    }
}

// return all our data to an AJAX call
echo json_encode($data);