(function () {

    angular
        .module('checkin')
        .controller('CheckinController', [
            'checkinService', '$mdSidenav', '$mdBottomSheet', '$mdUtil', '$log', '$q', '$mdDialog', '$http',
            CheckinController
        ]);

    function CheckinController(checkinService, $mdSidenav, $mdBottomSheet, $mdUtil, $log, $q, $mdDialog, $http) {
        var self = this;

        self.selected = null;
        self.users = [];
        self.userid = '';
        self.firstname = '';
        self.lastname = '';
        self.selectUser = {};
        self.toggleList = toggleUsersList;
        self.showUserInfo = showUserInfo;
        self.checkinUser = checkinUser;
        self.cameraReader = false;

        // Load all registered users

        checkinService
            .loadAllUsers()
            .then(function (users) {
                self.users = users.data;
                /*$log.info(self.users);
                self.selected = users;
                $log.info(self.selected);*/
            });

        // *********************************
        // Internal methods
        // *********************************
        self.openLeftMenu = function () {
            $mdSidenav('left').toggle();
        };

        self.toggleLeft = buildToggler('left');
        self.toggleRight = buildToggler('right');
        /**
         * Build handler to open/close a SideNav; when animation finishes
         * report completion in console
         */
        function buildToggler(navID) {
            var debounceFn =  $mdUtil.debounce(function(){
                $mdSidenav(navID)
                    .toggle()
                    .then(function () {
                        $log.debug("toggle " + navID + " is done");
                    });
            },200);
            return debounceFn;
        }

        /**
         * First hide the bottomsheet IF visible, then
         * hide or Show the 'left' sideNav area
         */
        function toggleUsersList() {
            var pending = $mdBottomSheet.hide() || $q.when(true);

            pending.then(function () {
                $mdSidenav('left').toggle();
            });
        }

        /**
         * Callback for QR Scanner
         * @param data
         */
        self.onSuccess = function(data) {
            if(data.length > 0) {
                self.userid = data;
            }
            console.log(data);
        };

        self.onError = function(error) {
            console.log(error);
        };

        self.onVideoError = function(error) {
            console.log(error);
        };

        /**
         * Show the User Card Info
         * @param event
         * @param obj
         */
        function showUserInfo(event, obj) {
            var cont = 'Full Name: ' + obj.firstname + ' ' + obj.lastname + ' / ' + 'Organization: ' + obj.organization + ' / ' + 'City: ' + obj.city;
            $mdDialog.show(
                $mdDialog.alert()
                    .title('VIP Detail Info')
                    .content(cont)
                    .ariaLabel('Secondary click demo')
                    .ok('Close')
                    .targetEvent(event)
            );
        };

        function checkinUser(event, obj) {
            if(obj.present) {
                $mdDialog.show(
                    $mdDialog.alert()
                        .title('Secure Information')
                        .content('This Person is already checked at ' + obj.stamp)
                        .ariaLabel('Secure Warning')
                        .ok('Close')
                        .targetEvent(event)
                );
            } else {
                self.selectUser = obj;
                //$log.info(self.selectUser);
                $http({
                    method  : 'POST',
                    url     : '../checkinvip.php',
                    data    : $.param(self.selectUser),
                    headers : { 'Content-Type': 'application/x-www-form-urlencoded' } // set the headers so angular passing info as form data (not request payload)
                })
                    .success(function(data) {
                        if (!data.success) {
                            $log.info(data.message);
                        } else {
                            self.selectUser = {};
                            obj.stamp = data.person.stamp;
                            $log.info(data.message);
                        }
                    });
            }
            return false;
        };

    }

})();
