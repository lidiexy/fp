(function(){
  'use strict';

  angular.module('checkin')
         .service('checkinService', ['$q', '$http', CheckinService]);

  function CheckinService($q, $http){
    // Promise-based API
    return {
      loadAllUsers : function() {
        var users = $http.get('../../data/vip.json');
        // Simulate async nature of real remote calls
        return users;
      }
    };
  }

})();
