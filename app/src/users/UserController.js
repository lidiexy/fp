(function () {

    angular
        .module('users')
        .directive('ngPrint', [printDirective])
        .controller('UserController', [
            'userService', '$mdSidenav', '$mdBottomSheet', '$mdUtil', '$log', '$q',
            UserController
        ]);

    function printDirective() {
        var printSection = document.getElementById("printSection");

        function printElement(elem) {
            // clones the element you want to print
            var domClone = elem.cloneNode(true);
            if (!printSection) {
                printSection = document.createElement("div");
                printSection.id = "printSection";
                document.body.appendChild(printSection);
            } else {
                printSection.innerHTML = "";
            }
            printSection.appendChild(domClone);
        }

        function link(scope, element, attrs) {
            element.on("click", function () {
                var elemToPrint = document.getElementById(attrs.printElementId);
                if (elemToPrint) {
                    printElement(elemToPrint);
                    window.print();
                }
            });
        }

        return {
            link: link,
            restrict: "A"
        };
    }

    function UserController(userService, $mdSidenav, $mdBottomSheet, $mdUtil, $log, $q) {
        var self = this;

        self.selected = null;
        self.users = [];
        self.selectUser = selectUser;
        //self.toggleList = toggleUsersList;
        //self.showContactOptions = showContactOptions;

        // Load all registered users

        userService
            .loadAllUsers()
            .then(function (users) {
                self.users = [].concat(users);
                self.selected = users[0];
            });

        // *********************************
        // Internal methods
        // *********************************
        self.openLeftMenu = function () {
            $mdSidenav('left').toggle();
        };

        self.toggleLeft = buildToggler('left');
        self.toggleRight = buildToggler('right');
        /**
         * Build handler to open/close a SideNav; when animation finishes
         * report completion in console
         */
        function buildToggler(navID) {
            var debounceFn =  $mdUtil.debounce(function(){
                $mdSidenav(navID)
                    .toggle()
                    .then(function () {
                        $log.debug("toggle " + navID + " is done");
                    });
            },200);
            return debounceFn;
        }

        /**
         * First hide the bottomsheet IF visible, then
         * hide or Show the 'left' sideNav area
         */
        /*function toggleUsersList() {
            var pending = $mdBottomSheet.hide() || $q.when(true);

            pending.then(function () {
                $mdSidenav('left').toggle();
            });
        }*/

        /**
         * Select the current avatars
         * @param menuId
         */
        function selectUser(user) {
            self.selected = angular.isNumber(user) ? $scope.users[user] : user;
            self.toggleList();
        };

    }

})();
